#!/bin/bash

# para compilar: ./nombre_script.sh archivo.cds.fasta > salida_carpeta_programa

echo -e "\n\nInforme de ensablado"

head -n2 < summary.txt

sed -n '7,43p'  summary.txt > table

#echo -e "\n\nlargos de los ensambles"
#echo "#Seq_id        Length(bp) Type"
#awk '{print $1 "\t" $4 "\t" $6}' table

echo -e "\n\nCDS"
echo "Lengh(bp)	Type   Gene_name  Gene_prodcut"
grep CDS table | awk '{print $4 "\t\t" $6 "\t" $7 "\t" $8 }' > cds.txt
cat cds.txt

echo -e "\nCatidad de CDS"
wc -l cds.txt


echo -e "\n\ntRNA"
echo "Lengh(bp)	Type   Gene_name	 Gene_prodcut"
grep tRNA table | awk '{print $4 "\t\t" $6 "\t" $7 "\t" $8 }' > trna.txt
cat trna.txt
echo -e "\nCantidad de tRNA"
wc -l trna.txt


echo -e "\n\n\nTripletes de inincio y final de genes codificantes"
echo -e "CDS	T. inicio	T. final"

grep "^>" $1 | awk -F ";" '/;/ {print $(NF-2)}' > pt1.txt

grep -v "^>" $1 | sed -r 's/^([ATCGatcg]{3}).*([ATCGatcg]{3})$/\1\t\t\2/'> inincio_y_final.txt



paste pt1.txt inincio_y_final.txt > final_final.txt
cat final_final.txt

rm pt1.txt inincio_y_final.txt trna.txt cds.txt table final_final.txt
