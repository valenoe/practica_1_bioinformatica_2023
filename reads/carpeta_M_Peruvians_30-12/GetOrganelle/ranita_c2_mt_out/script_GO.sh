#!/bin/bash

# para correr: ./script_GO.sh archivo.csv > out.txt
# asegurese de limpiar el csv primero
# esto solo lo ordena 

awk '{print $4}' $1 > details

#sed 's/>>/\n/g' details > cambio.txt
sed 's/(/,/g' details > pt1.txt 

awk -F "," '/,/ {print $(NF-1)}' pt1.txt > pt2.txt



sed 's/-/\t/g' pt2.txt > pt3.txt

awk '{print $1 "\t" $2 "\t" $3}' $1 > pt4.txt
sed -i '1d' pt4.txt
paste pt4.txt pt3.txt > pt5.txt

awk '{print $1 "\t" $2 "\t" $3 "\t" $4 "\t" $5 "\t" $5-$4+1}' pt5.txt > pt6.txt


echo "EDGE	database	loci	start	end	lengh(pb)"
cat pt6.txt

rm details pt1.txt pt2.txt pt3.txt pt4.txt pt5.txt


grep -v rrn pt6.txt > cds.txt
echo -e "\ntotal CDS"

wc -l cds.txt
echo -e "\nCDS"
awk '{print $1 "\t" $3 "\t" $6}' cds.txt > sum_cds.txt
echo -e "EDGE	loci	lenght(pb)"
cat sum_cds.txt

rm pt6.txt cds.txt sum_cds.txt
#awk -F "(" '/^\(/ {print $NF}' > pt1.txt
