source activate /home/valery/anaconda3/envs/mitozEnv/lib/python3.8/site-packages/mitoz/mitoz3.4

mitoz all \
--outprefix resultado_filtrado \
--thread_number 8 \
--clade Chordata \
--genetic_code 2 \
--species_name "Pleurodema thaul" \
--fq1 test_1.fastq \
--fq2 test_2.fastq \
--fastq_read_length 151 \
--data_size_for_mt_assembly 0.3 \
--assembler megahit \
--kmers_megahit 59 79 99 119 141 \
--memory 50 \
--requiring_taxa Chordata

